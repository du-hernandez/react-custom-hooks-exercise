import React from 'react';
import '../node_modules/bulma/bulma.sass'; // Para este propósito es necesario instalar la librería `node-sass`
import Blog from './components/blog'
import { articles } from './data';

const App = () => <Blog articles={articles} />;

export default App;
