import React from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  ModalBackground,
  ModalContent,
  ModalClose,
  Title
} from 'bloomer'

const ModalView = ({
  isActive, children, title, handleClose
}) => {

  const handleCloseAction = () => handleClose(false);

  return (
    <Modal isActive={isActive}>
      <ModalBackground onClick={handleCloseAction} />
      <ModalContent
        style={{ backgroundColor: 'white', padding: '2rem', maxWidth: '100vw' }}
      >
        <Title isSize={6}>{title}</Title>
        {children}
      </ModalContent>
      <ModalClose onClick={handleCloseAction} />
    </Modal>
  );
}

ModalView.propTypes = {
  isActive: PropTypes.bool.isRequired,
  children: PropTypes.element.isRequired,
  title: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired
};

export default ModalView;