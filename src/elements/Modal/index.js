import React from 'react';
import PropTypes from 'prop-types';
import ModalView from './view';

const Modal = ({ isActive, children, title, handleClose }) => (
  <ModalView
    isActive={isActive}
    title={title}
    handleClose={handleClose}
    children={children}
  />
);

Modal.propTypes = {
  isActive: PropTypes.bool.isRequired,
  children: PropTypes.element.isRequired,
  title: PropTypes.string.isRequired,
  handleClose: PropTypes.func.isRequired
};

export default Modal;