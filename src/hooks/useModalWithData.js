import { useState } from 'react';
import { useModal } from './useModal';

export const useModalWithData = (
  initialData = { id: 'Select a article', title: '', body: '' },
  initialMode = false
) => {
  const [isOpened, setIsOpened] = useModal(initialMode);
  const [data, setData] = useState(initialData);

  const setModalVisible = isModalOpened => {
    setIsOpened(isModalOpened);
    /**
     * A continuación, si el modal es cerrado
     */
    if (!isModalOpened) setData(null);
  };

  return [isOpened, setModalVisible, data, setData];
};