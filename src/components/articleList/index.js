import React from 'react';
import PropTypes from 'prop-types';

const ArticleList = ({ articles, setModalData, setIsModalOpened }) => {

  const handleViewArticle = (article) => {
    setModalData(article);
    setIsModalOpened(true);
  }

  return (
    <ul>
      {articles.map(article => {
        const { id, title } = article;
        return (
          <li key={`article--${id}`}>
            {title}
            <button onClick={() => handleViewArticle(article)}>
              View article
            </button>
          </li>
        );
      })}
    </ul>
  );
}

ArticleList.prototype = {
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};

export default ArticleList;