import React from 'react';
import PropTypes from 'prop-types';

const emtyArticle = {
  title: 'Untitled',
  body: 'Without body'
}

const ArticleDetail = ({ article = emtyArticle }) => {
  
  if (!article) return null;

  const { title, body } = article;

  return (
    <div>
      <h4>{title}</h4>
      <div>{body}</div>
    </div>
  );
};

ArticleDetail.prototype = {
  article: PropTypes.shape({
    title: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired
  })
};

export default ArticleDetail;