import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from '../../elements'
import { useModalWithData } from '../../hooks/useModalWithData';
import ArticleDetail from '../articleDetail';
import ArticleList from '../articleList';

const Blog = ({ articles }) => {

  const [
    isOpened,
    setModalVisible,
    data,
    setData
  ] = useModalWithData();

  return (
    <div>
      <div style={{ padding: '1rem' }}>
        <Modal
          title='useModalWithData'
          isActive={isOpened}
          handleClose={setModalVisible}
        >
          <ArticleDetail article={data} />
        </Modal>

        <ArticleList
          articles={articles}
          setModalData={setData}
          setIsModalOpened={setModalVisible}
        />
      </div>
    </div>
  );

};

Blog.prototype = {
  articles: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};

export default Blog;